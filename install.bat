::  Installation script to install ENIMAS Software

@echo off

timeout /t 3 /nobreak

:: check if script is already run with admin permissions
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

if '%errorlevel%' NEQ '0' (
goto UACPrompt
) else ( goto gotAdmin )

:: Execute this script with admin permissions (this will pop up an elevation prompt)
:UACPrompt
echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
echo UAC.ShellExecute "%~s0", "", "", "runas", 1 >> "%temp%\getadmin.vbs"
"%temp%\getadmin.vbs"
exit /B

:gotAdmin
if exist "%temp%\getadmin.vbs" ( del "%temp%\getadmin.vbs" )


:: create Windows Programs Folder
if exist "C:\Program Files\ENIMAS\" (
    echo ENIMAS is already installed. The program is being updated ...
    rmdir /s /q "C:\Program Files\ENIMAS\src\"
    if exist "C:\Program Files\ENIMAS\src\" (
        echo Updating ENIMAS failed. Please close all other processes that are using ENIMAS currently and then restart this script.
        pause 
        goto :EOF
    )
) else (
    echo Creating program folder: C:\Program Files\ENIMAS\
    mkdir "C:\Program Files\ENIMAS\" 
)

cd "C:\Program Files\ENIMAS\"

:: grant permisson to all local users to all files and subfolders
icacls "C:\Program Files\ENIMAS" /t /q /grant *S-1-2-0:(OI)(CI)F >nul 2>&1 
echo.

:: install Python without user interaction in UI if python 3.10 isn't installed
if not exist "C:\Program Files\Python310\python.exe" (
    echo Installing Python 3.10 ...
    Powershell -command "curl https://www.python.org/ftp/python/3.10.8/python-3.10.8-amd64.exe -o python-installer.exe"
    if not exist .\python-installer.exe (
        echo Installation failed!
        echo Please make sure to have an internet connection.
        echo.
        pause
        exit
    )
    python-installer.exe /passive InstallAllUsers=1 PrependPath=1 Include_doc=0 Include_test=0 Include_tcltk=0
    del python-installer.exe
    echo Successfully installed Python 3.10
    echo.
) else ( echo Python 3.10 already installed. Skipping installation.)


if not exist "C:\Program Files\ENIMAS\enimas-main" (
:: load the source code form gitlab
echo Loading source script from GitLab ...
PowerShell -command "curl https://gitlab.kit.edu/kit/iai/ber/enimas/-/archive/main/enimas-main.zip -o enimas.zip"
if not exist .\enimas.zip (
    echo Installation failed!
    echo Please make sure to have an internet connection.
    echo.
    pause
    exit
)
tar -xf enimas.zip
del enimas.zip
echo Successfully loaded source script.
)


:: create a virtual environment and install all dependencies
ren enimas-main src
cd src\

echo Creating virtual environment ...
"C:\Program Files\Python310\python.exe" -m venv venv
echo Successfully created virtual environment

echo.
echo Installing python dependencies ...
.\venv\Scripts\pip install -r .\requirements.txt
echo All dependencies have been installed


:: install the Arducam driver:
:: from https://github.com/ArduCAM/ArduCAM_USB_Camera_Shield
echo Installing ArduCam Driver ...
echo.
if "%PROCESSOR_ARCHITECTURE%" == "x86" goto x86
if "%PROCESSOR_ARCHITECTURE%" == "AMD64" goto x64

:x64
echo %PROCESSOR_ARCHITECTURE% 
C:\Windows\System32\pnputil.exe /add-driver .\camera-driver\x64\cyusb3.inf /install
goto end

:x86
echo %PROCESSOR_ARCHITECTURE% 
C:\Windows\System32\pnputil.exe /add-driver .\camera-driver\x86\cyusb3.inf /install
goto end

:end
echo Driver installed successfully
echo.

:: create a hard link to the ENIMAS.exe executable on the user desktop
cd ..
if not exist .\ENIMAS.exe (
    >nul move .\src\ENIMAS.exe .
)  else ( del .\ENIMAS.exe
	>nul move .\src\ENIMAS.exe .
    )

if not exist "C:\Users\Public\Desktop\ENIMAS.exe" (
    >nul mklink /H "C:\Users\Public\Desktop\ENIMAS.exe" .\ENIMAS.exe
    echo created link to Program Executable on public Desktop.
) else ( del C:\Users\Public\Desktop\ENIMAS.exe
	>nul mklink /H "C:\Users\Public\Desktop\ENIMAS.exe" .\ENIMAS.exe
	echo updated link to Program Executable on public Desktop.
	)

if not exist "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\ENIMAS.exe" (
    >nul mklink /H "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\ENIMAS.exe" .\ENIMAS.exe
    echo created link to Program Executable on Windows Start Menu.
) else (
    del "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\ENIMAS.exe"
	>nul mklink /H "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\ENIMAS.exe" .\ENIMAS.exe
	echo updated link to Program Executable on Windows Start Menu.
	)

if exist "C:\Program Files\ENIMAS\lenses.json" (
    echo Old lenses file found. Replacing default lenses file with old file ...
    cd C:\Program Files\ENIMAS\
    copy /y .\lenses.json .\src\
)

echo.
echo Installation complete.
echo.

pause
