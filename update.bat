copy .\lenses.json ..


cd..
:: load the source code form gitlab
echo Loading source script from GitLab ...
PowerShell -command "curl https://gitlab.kit.edu/kit/iai/ber/enimas/-/archive/main/enimas-main.zip -o enimas.zip"
if not exist .\enimas.zip (
    echo Installation failed!
    echo Please make sure to have an internet connection.
    echo.
    pause
    exit
)
tar -xf enimas.zip
del enimas.zip
echo Successfully loaded source script.

copy /y .\enimas-main\install.bat .

timeout /t 3 /nobreak

:: starting update

./install.bat