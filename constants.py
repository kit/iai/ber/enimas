import os

# ungefähre Motorgeschwindigkeit in Umdrehungen/s
DEFAULT_SPEED = 5
LOW_SPEED = 2
MAX_SPEED = 6

# linear Achse länge in mm
AXIS_LENGHT = 343
"""
lenght linear axis: 409mm
lenght slide: 66mm
max lenght zero-point to highest-slide-point: 343mm

lenght zero-point to base-plate: 422mm
highest-slide-point to lowest-sensor-point: 59mm
"""

HELICON_PATH = "undefined"

MICROSTEPS = 16
ROTATION_HEIGHT = 10

STACK_SPEED = 50

DEFAULT_NUM_OF_STACKS = 10
MIN_NUM_OF_STACKS = 2
MAX_NUM_OF_STACKS = 100

DEFAULT_STACK_STEP_SIZE = 1.0
MIN_STACK_STEP_SIZE = 0.01
MAX_STACK_STEP_SIZE = AXIS_LENGHT

DEFAULT_BASE_DIR = os.path.join(os.environ["USERPROFILE"], "Pictures", "ENIMAS")
IMG_EXTENSION = "tiff"

EXP_TIME_MIN = 1
EXP_TIME_MAX = 330000

GAIN_MIN = 1
GAIN_MAX = 26

WB_SCALING = 100
WB_MIN =  WB_SCALING
WB_MAX = 16 * WB_SCALING
