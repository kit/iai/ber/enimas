import sys
import subprocess
from requests import get
from time import sleep, time
from serial import Serial
from serial.tools.list_ports import comports
from Arducam.Arducam import ArducamCamera

from logging import basicConfig, getLogger, DEBUG, INFO
basicConfig(filename="debug.log", filemode="w+", level=DEBUG)

logger = getLogger()
logger.setLevel(DEBUG)

from PyQt5.QtWidgets import QApplication, QPushButton, QDialog, QMessageBox
from PyQt5.QtCore import Qt
from PyQt5 import uic

from UserInterface.ui import MainWindow
from MotorController.motorcontroller import Axis
import constants
import os


def read_config():
    with open("config.txt", "r") as config_file:
        config = {} 
        for line in config_file.readlines():
            if line.startswith("#") or "=" not in line:
                continue
            
            prop, value = line.split("=", 1)
            prop, value = prop.rstrip().lstrip(), value.rstrip().lstrip()
            if not prop or not value: continue
            
            config[prop] = value
    return config

def write_config(params: dict):
    if not "version" in params:
        params["version"] = "1.0.0"
    
    params["helicon-path"] = constants.HELICON_PATH
    params["base-directory"] = constants.DEFAULT_BASE_DIR
    params["image-extension"] = constants.IMG_EXTENSION
    params["stack-speed"] = constants.STACK_SPEED
    params["number-of-stacks"] = constants.DEFAULT_NUM_OF_STACKS
    params["stack-step-size"] = constants.DEFAULT_STACK_STEP_SIZE
    
    with open("config.txt", "w") as config_file:
        for key, val in params.items() :
            config_file.write(f"{key}={val}\n")

def save_config(cfg: dict):
    """saves config params as python attributes"""
    try:
        constants.__setattr__("IMG_EXTENSION", cfg["image-extension"]) if "image-extension" in cfg else None
        helicon_path = cfg["helicon-path"] if "helicon-path" in cfg else "undefined"
        constants.__setattr__("HELICON_PATH", helicon_path)
        constants.__setattr__("STACK_SPEED", int(cfg["stack-speed"])) if "stack-speed" in cfg else None
        constants.__setattr__("DEFAULT_NUM_OF_STACKS", int(cfg["number-of-stacks"])) if "number-of-stacks" in cfg else None
        constants.__setattr__("DEFAULT_STACK_STEP_SIZE", float(cfg["stack-step-size"])) if "stack-step-size" in cfg else None
        constants.__setattr__("DEFAULT_BASE_DIR", cfg["base-directory"]) if "base-directory" in cfg else None
    except:
        # parameter is missing or ValueError, this invalid data can be ignored
        pass

def get_gitlab_version() -> str:
    """Gets the enimas version that is on the online gitlab repository"""
    try:
        response = get("https://gitlab.kit.edu/kit/iai/ber/enimas/-/raw/main/config.txt")
        config_file = response.content.decode("utf-8")
        
        for param in config_file.split("\n"):
            if "version=" in param:
                return param.split("=")[1]
    except:
        logger.info("Could not get gitlab online version")
    
    return None

def check_version(current_version: str):
    """Compares this version to the version in the online gitlab repository"""
    online_version = get_gitlab_version()
    if online_version and online_version != current_version:
        logger.debug(f"new version available: {online_version}, old: {current_version}")
        prompt_to_update(current_version, online_version)

def prompt_to_update(cur_ver, new_ver):
    app = QApplication(sys.argv)
    
    m = QMessageBox()
    ans = QMessageBox.question(m, "ENIMAS: a new version is available", f"A new ENIMAS version is available: {new_ver}\nCurrent version: {cur_ver}\n\nWould you like to update ENIMAS?")
    if ans == QMessageBox.Yes:
        logger.info("Program is going to update...")
        subprocess.run(r".\update.bat")
        sys.exit()
    else:
        logger.info("User denied update. Program continuing")

def open_camera() -> ArducamCamera:
    camera = ArducamCamera()
    cfg_file = "Arducam/IMX477_2Lane_4032x3040_RAW8_A.cfg"

    t0 = time()
    opened = camera.openCamera(cfg_file)
    while not opened:
        opened = camera.openCamera(cfg_file)

        if time() - t0 > 3.0:
            # timeout after 5s
            logger.error("Failed to open camera.")
            return None

    camera.start()
    return camera

def find_arduino() -> Serial:
    list_of_ports = list(comports())
    logger.debug(f"COM Ports: {list(map(str, list_of_ports))}")
        
    if not list_of_ports:
        return None
    
    if "Arduino" in str(list(map(str, list_of_ports))):
        for port, desc, hwid in list_of_ports:
            
            if "Arduino" in desc:
                logger.debug(f"found arduino: {port}")
                return Serial(port, baudrate=115200, timeout=.05)
                
    for port, desc, hwid in list_of_ports:
        logger.debug(f"{port=}, {desc=}, {hwid=}") 
        try:
            con = Serial(port, baudrate=115200, timeout=.05)
            sleep(2)
            logger.debug(f"written to {port}")
            t0=time()
            
            con.write(b"arduino")
            while time()- t0 < .1:
                ans = con.readline()
                logger.debug(f"answer: {ans}")
                
                if ans.decode("ascii").strip() == "yes":
                    logger.debug(f"{port} won")
                    return con
                
            con.close()
        
        except Exception as e:
            logger.debug(f"error while trying to connect to a COM port: {e}")
    
    logger.error("Failed to connect to the Arduino.")
    return None


class StartWindow(QDialog):
    def __init__(self):
        super().__init__()
        uic.loadUi("UserInterface/logo_screen.ui", self)
        self.setWindowFlag(Qt.WindowType.FramelessWindowHint)


class NoConnectionWindow(QDialog):
    def __init__(self):
        super().__init__()
        uic.loadUi("UserInterface/connection_error.ui", self)
        self.retry_bttn = self.findChild(QPushButton, "retry_bttn")
        self.close_bttn = self.findChild(QPushButton, "close_bttn")
        self.retry_bttn.clicked.connect(self.close)
        self.close_bttn.clicked.connect(exit)



if __name__ == "__main__":
    cfg = read_config()
    if "version" in cfg:
        check_version(cfg["version"])
    if "base-directory" in cfg:
        if not os.path.isdir(cfg["base-directory"]):
            del cfg["base-directory"]                        
    save_config(cfg)
    
    
    app = QApplication(sys.argv)
    
    start_window = StartWindow()
    start_window.show()
    
    arduino = find_arduino()
    camera = open_camera()
    connection_window = None
    
    while arduino is None or camera is None:
        connection_window = NoConnectionWindow()
        connection_window.exec()
        arduino = find_arduino() if arduino is None else arduino
        camera = open_camera() if camera is None else camera
    
    if connection_window:
        connection_window.close()
    start_window.close()
    
    axis = Axis(arduino)
    window = MainWindow(axis, camera)
    window.show()
    rtn = app.exec()
    logger.info("app executed")
    write_config(cfg)
    
    sys.exit(rtn)
