
import os
import cv2
import numpy
import constants
import subprocess
from logging import getLogger, basicConfig, INFO, DEBUG
from threading import Thread
from UserInterface.method_selection import prompt_user_fuse_mode

basicConfig(level=DEBUG)
logger = getLogger("stacker")
logger.setLevel(DEBUG)


def fuse_stacks(dir_path, img_name, fuse_status_signal):
    helicon_path = constants.HELICON_PATH
    logger.debug(f"{helicon_path=}")
    
    if helicon_path == "undefined":
        # The user hat not yet decided between helicon and selfmade
        prompt_user_fuse_mode(_fuse_stacks, dir_path, img_name, fuse_status_signal)
    else:
        # a decision was saved
        _fuse_stacks(helicon_path, dir_path, img_name, fuse_status_signal)


def _fuse_stacks(helicon_path, dir_path, img_name, fuse_status_signal):
    logger.debug(f"_fuse_stacks: {helicon_path=}")
    fuse_status_signal.emit("Fusing stack...")
    constants.HELICON_PATH = helicon_path
    
    if helicon_path == "None":
        # Fuse stacks with selfmade version
        stacker = Stacker(dir_path, img_name, fuse_status_signal)
        stacker.start()
    else:
        # Fuse stacks with Helicon Focus
        stacker = HeliconStacker(helicon_path, dir_path, img_name, fuse_status_signal)
        stacker.start()

class HeliconStacker(Thread):
    def __init__(self,helicon_path: str, dir_path: str, stacked_img_name: str, fuse_status_signal) -> None:
        super().__init__()
        logger.info("fuze with helicon")
    
        self.helicon_path = helicon_path
        self.dir_path = dir_path
        self.stacked_img_name = stacked_img_name
        self.fuse_status_signal = fuse_status_signal


    def run(self):
        logger.info("fuze with helicon")
        self.fuse_status_signal.emit("Fusing stack with Helicon focus...")
        
        command = f'"{self.helicon_path}" -silent "{self.dir_path}" -save:"{self.stacked_img_name}"'
        logger.debug(command)
        subprocess.run(command, shell=True, encoding="utf-8")
        self.fuse_status_signal.emit("Fusing stack finished.")

class Stacker(Thread):
    _BLUR_SIZE = 1
    _SIGMAX = 0
    _KERNEL_SIZE = 7
    
    def __init__(self, raw_img_dir_path: str, stacked_img_name: str, fuse_status_signal) -> None:
        super().__init__()
        logger.debug("fuze selfmade init")
        
        self.raw_img_dir_path = raw_img_dir_path
        self.stacked_img_name = stacked_img_name
        self.fuse_status_signal = fuse_status_signal
        
    def run(self):
        logger.debug("stacking running")
        self.fuse_status_signal.emit("Preparing to fuse stacks...")
        
        raw_img_list = []
        gray_img_list = []
        for img_name in os.listdir(self.raw_img_dir_path):
            img_path = os.path.join(self.raw_img_dir_path, img_name)
            image = cv2.imread(img_path)
            raw_img_list.append(image)
            
            gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            gray_img_list.append(gray_image)
            
        logger.debug("images ready")
        self.fuse_status_signal.emit("Aligning stack images...")
        aligned_images = self.align_images(gray_img_list)
        
        print("images aligned")
        stacked_img = self.focus_stack(raw_img_list, aligned_images)
        
        cv2.imwrite(self.stacked_img_name, stacked_img)
        self.fuse_status_signal.emit("Fusing complete.")
        logger.debug("stacking complete")
        
    def align_images(self, raw_img_list: list) -> list:
        """Alignes images"""
        
        aligned_imgs = [raw_img_list[0]]
        for i, img2 in enumerate(raw_img_list[1:]):
            logger.debug(f"aligning img {i+1}")
            self.fuse_status_signal.emit(f"Aligning stack image {i+1}...")
            img1 = aligned_imgs[i]
            
            sz = img1.shape
            warp_mode = cv2.MOTION_TRANSLATION
            warp_matrix = numpy.eye(2, 3, dtype=numpy.float32)

            number_of_iterations = 5000
            termination_eps = 1e-10
            criteria = (int(cv2.TERM_CRITERIA_EPS / cv2.TERM_CRITERIA_COUNT), number_of_iterations, termination_eps)
            
            _, warp_matrix = cv2.findTransformECC(img1, img2, warp_matrix, warp_mode, criteria)
            img2_aligned = cv2.warpAffine(img2, warp_matrix, (sz[1], sz[0]), flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP)
            
            aligned_imgs.append(img2_aligned)
        
        return aligned_imgs
        
    def focus_stack(self, images, gray_images):
        """
        Find the sharpest area of each of the superimposed images 
        and generates an image from the different sharp areas.
        """
        
        laplacians_img = []
        for raw_image in gray_images:
            self.fuse_status_signal.emit("Fusing stacks...")
            blurred = cv2.GaussianBlur(raw_image, (self._BLUR_SIZE, self._BLUR_SIZE), self._SIGMAX)
            laplacian = cv2.Laplacian(blurred, cv2.CV_64F, ksize=self._KERNEL_SIZE)

            laplacians_img.append(laplacian)
        
        laplacians_img = numpy.asarray(laplacians_img)
        output = numpy.zeros(shape=images[0].shape, dtype=images[0].dtype)

        abs_laplacians_img = numpy.absolute(laplacians_img)
        maxima = abs_laplacians_img.max(axis=0)
        bool_mask = abs_laplacians_img == maxima
        mask = bool_mask.astype(numpy.uint8)
        
        for i, img in enumerate(images):
            output = cv2.bitwise_not(img, output, mask=mask[i])
        
        return 255 - output
