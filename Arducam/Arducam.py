"""source: https://github.com/ArduCAM/ArduCAM_USB_Camera_Shield_Python_Demo/blob/main/Arducam.py"""


import constants
import threading

import ArducamSDK
from Arducam.utils import *


class ArducamCamera(object):
    def __init__(self):
        self.isOpened = False
        self.running_ = False
        self.signal_ = threading.Condition()
        pass

    def openCamera(self, fname, index=0):
        self.isOpened, self.handle, self.cameraCfg, self.color_mode = camera_initFromFile(
            fname, index)

        return self.isOpened

    def start(self):
        if not self.isOpened:
            raise RuntimeError("The camera has not been opened.")
        
        self.running_ = True
        ArducamSDK.Py_ArduCam_setMode(self.handle, ArducamSDK.CONTINUOUS_MODE)
        self.capture_thread_ = threading.Thread(target=self.capture_thread)
        self.capture_thread_.daemon = True
        self.capture_thread_.start()
    
    def read(self, timeout=1500):
        if not self.running_:
            raise RuntimeError("The camera is not running.")

        if ArducamSDK.Py_ArduCam_availableImage(self.handle) <= 0:
            with self.signal_:
                self.signal_.wait(timeout/1000.0)

        if ArducamSDK.Py_ArduCam_availableImage(self.handle) <= 0:
            return (False, None, None)

        ret, data, cfg = ArducamSDK.Py_ArduCam_readImage(self.handle)
        ArducamSDK.Py_ArduCam_del(self.handle)
        size = cfg['u32Size']
        if ret != 0 or size == 0:
            return (False, data, cfg)
    
        return (True, data, cfg)

    def stop(self):
        if not self.running_:
            raise RuntimeError("The camera is not running.")

        self.running_ = False
        self.capture_thread_.join()

    def closeCamera(self):
        if not self.isOpened:
            raise RuntimeError("The camera has not been opened.")

        if (self.running_):
            self.stop()
        self.isOpened = False
        ArducamSDK.Py_ArduCam_close(self.handle)
        self.handle = None

    def capture_thread(self):
        ret = ArducamSDK.Py_ArduCam_beginCaptureImage(self.handle)

        if ret != 0:
            self.running_ = False
            raise RuntimeError("Error beginning capture, Error : {}".format(GetErrorString(ret)))

        # print("Capture began, Error : {}".format(GetErrorString(ret)))
        
        while self.running_:
            ret = ArducamSDK.Py_ArduCam_captureImage(self.handle)
            if ret > 255:
                # print("Error capture image, Error : {}".format(GetErrorString(ret)))
                if ret == ArducamSDK.USB_CAMERA_USB_TASK_ERROR:
                    break
            elif ret > 0:
                with self.signal_:
                    self.signal_.notify()
            
        self.running_ = False
        ArducamSDK.Py_ArduCam_endCaptureImage(self.handle)

    def read_reg_values(self):
        err, exp1 = ArducamSDK.Py_ArduCam_readSensorReg(self.handle, 0x202)
        err, exp2 = ArducamSDK.Py_ArduCam_readSensorReg(self.handle, 0x203)
        hts, pix_clk_hz = 12740, 840000000
        val = (exp1 << 8) | exp2
        exp_time = int(val*hts*1e9/(1000*pix_clk_hz))

        err, gain1 = ArducamSDK.Py_ArduCam_readSensorReg(self.handle, 0x0204)
        err, gain2 = ArducamSDK.Py_ArduCam_readSensorReg(self.handle, 0x0205)
        val = (gain1 << 8) | gain2
        gain = int(1024/(1024-val))

        err, wb_r_upper = ArducamSDK.Py_ArduCam_readSensorReg(self.handle, 0x0210)
        err, wb_r_lower = ArducamSDK.Py_ArduCam_readSensorReg(self.handle, 0x0211)
        wb_r = wb_r_upper + wb_r_lower / 256

        err, wb_gr_upper = ArducamSDK.Py_ArduCam_readSensorReg(self.handle, 0x020E)
        err, wb_gr_lower = ArducamSDK.Py_ArduCam_readSensorReg(self.handle, 0x020F)
        wb_gr = wb_gr_upper + wb_gr_lower / 256

        err, wb_b_upper = ArducamSDK.Py_ArduCam_readSensorReg(self.handle, 0x0212)
        err, wb_b_lower = ArducamSDK.Py_ArduCam_readSensorReg(self.handle, 0x0213)
        wb_b = wb_b_upper + wb_b_lower / 256

        return exp_time, gain, int(wb_r*constants.WB_SCALING), int(wb_gr*constants.WB_SCALING), int(wb_b*constants.WB_SCALING)

    def set_exposure_time(self, val: int):
        '''Set the exposure time in th unit of [µs]
        MIN_VALUE   = 1
        MAX_VALUE   = 330000
        STEP        = 1
        '''
        hts = 12740
        pix_clk_hz = 840000000
        exp = round(val*1000/(hts/pix_clk_hz*1e9))
        ArducamSDK.Py_ArduCam_writeSensorReg(self.handle, 0x0202, (exp & 0xFF00) >> 8)
        ArducamSDK.Py_ArduCam_writeSensorReg(self.handle, 0x0203, (exp & 0x00FF) >> 0)

    def set_analog_gain(self, val: int):
        '''Set the gain with a gain factor from 1 to 26'''
        gain = round(1024 - (1024 / val))
        # print('gain: ', gain)
        ArducamSDK.Py_ArduCam_writeSensorReg(self.handle, 0x0204, (gain & 0xFF00) >> 8)
        ArducamSDK.Py_ArduCam_writeSensorReg(self.handle, 0x0205, (gain & 0x00FF) >> 0)
   
    def set_wb_gains_factor(self, r_gain, gr_gain, b_gain):
        """
        Set the digital gain for the individual colors.
        Values range from 0x0100 to 0x0FFF

                7654 3210 
        XXXX XXXX XXXX XXXX
        UpperByte LowerByte


        Digital gain [times] = upper_byte + (lower_byte/256)

        upper_byte ranges from 1 -15
        lower_byte ranges from 0-255lower
        """

        ArducamSDK.Py_ArduCam_writeSensorReg(self.handle, 0x3FF9, 0)
        gr_gain, r_gain, b_gain = gr_gain/constants.WB_SCALING, r_gain/constants.WB_SCALING, b_gain/constants.WB_SCALING

        GAIN_GR_upper = int(gr_gain)
        GAIN_GR_lower = int(round((gr_gain-float(GAIN_GR_upper))*256))
        if GAIN_GR_lower == 256:
            GAIN_GR_lower = 255

        GAIN_GB_lower = GAIN_GR_lower
        GAIN_GB_upper = GAIN_GR_upper

        ArducamSDK.Py_ArduCam_writeSensorReg(self.handle, 0x020E, GAIN_GR_upper)
        ArducamSDK.Py_ArduCam_writeSensorReg(self.handle, 0x020F, GAIN_GR_lower)
        
        ArducamSDK.Py_ArduCam_writeSensorReg(self.handle, 0x0214, GAIN_GB_upper)
        ArducamSDK.Py_ArduCam_writeSensorReg(self.handle, 0x0215, GAIN_GB_lower)

        GAIN_R_upper = int(r_gain)
        GAIN_R_lower = int(round((r_gain-float(GAIN_R_upper))*256))

        ArducamSDK.Py_ArduCam_writeSensorReg(self.handle, 0x0210, GAIN_R_upper)
        ArducamSDK.Py_ArduCam_writeSensorReg(self.handle, 0x0211, GAIN_R_lower)

        GAIN_B_upper = int(b_gain)
        GAIN_B_lower = int(round((b_gain-float(GAIN_B_upper))*256))
    
        # print(f"WB: {GAIN_GR_upper}:{GAIN_GR_lower}, {GAIN_GB_upper}:{GAIN_GB_lower}, {GAIN_R_upper}:{GAIN_R_lower}, {GAIN_B_upper}:{GAIN_B_lower}")
        ArducamSDK.Py_ArduCam_writeSensorReg(self.handle, 0x0212, GAIN_B_upper)
        ArducamSDK.Py_ArduCam_writeSensorReg(self.handle, 0x0213, GAIN_B_lower)
