
import os
import sys
from PyQt5.QtWidgets import QApplication, QDialog, QPushButton, QLineEdit, QRadioButton, QButtonGroup, QFileDialog, QMessageBox
from PyQt5.QtCore import pyqtSignal
from PyQt5 import uic

# "C:\Program Files\Helicon Software\Helicon Focus 8\HeliconFocus.exe"


class MethodDialog(QDialog):
    decision_made = pyqtSignal(str)
    
    def __init__(self):
        super().__init__()
        uic.loadUi("UserInterface/stacking_method_dialog.ui", self)
        
        self._fuse_method = 0
        self._helicon_path = None
        
        self.helicon_radio_bttn = self.findChild(QRadioButton, "helicon_radio_bttn")
        self.selfmade_radio_bttn = self.findChild(QRadioButton, "selfmade_radio_bttn")
        self.button_group = QButtonGroup()
        self.button_group.addButton(self.helicon_radio_bttn)
        self.button_group.addButton(self.selfmade_radio_bttn)
        self.helicon_radio_bttn.toggled.connect(self.method_toggled)
        self.selfmade_radio_bttn.toggled.connect(self.method_toggled)
        
        self.ok_bttn = self.findChild(QPushButton, "ok_bttn")
        self.cancel_bttn = self.findChild(QPushButton, "cancel_bttn")
        self.ok_bttn.clicked.connect(self.ok_clicked)
        self.cancel_bttn.clicked.connect(self.close)
        
        self.helicon_path_lineedit = self.findChild(QLineEdit, "helicon_path_lineedit")
        self.browse_bttn = self.findChild(QPushButton, "browse_bttn")
        self.browse_bttn.clicked.connect(self.browse_path)
        
        self.auto_find_path()

    def auto_find_path(self):
        stdpath = r"C:\Program Files\Helicon Software\Helicon Focus 8\HeliconFocus.exe"
        if os.path.isfile(stdpath):
            self.helicon_path_lineedit.setText(stdpath)

    
    def browse_path(self):
        response = QFileDialog.getOpenFileName(
            parent=self,
            caption="Select the path to the Helicon Focus software executable (.exe)",
            directory=r"C:\Program Files",
            filter="Executable file (*.exe)"
        )
        
        self._helicon_path=response[0] 
        self.helicon_path_lineedit.setText(self._helicon_path)
        
    def method_toggled(self, bttn):
        bttn: QRadioButton = self.sender()
        if bttn.isChecked():
            if bttn == self.helicon_radio_bttn:
                self._fuse_method = 0
            if bttn == self.selfmade_radio_bttn:
                self._fuse_method = 1
    
    @staticmethod
    def check_helicon_path(path):
        if os.path.basename(path) != "HeliconFocus.exe" and os.path.isdir(path) and "HeliconFocus.exe" in os.listdir(path):
            # the parent folder was selected
            path = os.path.join(path, "HeliconFocus.exe")
            
        if not path or not os.path.isfile(path) or not "HeliconFocus.exe" in str(path):
            return False
            
        return path
    
    def ok_clicked(self):
        if self._fuse_method == 0:
            path = self.helicon_path_lineedit.text()
            
            path = self.check_helicon_path(path)
            
            if not path:
                QMessageBox.critical(self, "Helicon path is invalid", "The Helicon executable file path is invalid.\nPlease enter a valid path.")
                return 
            
            self.close()
            self.decision_made.emit(path)
        
        if self._fuse_method == 1:
            self.close()
            self.decision_made.emit("None")


def prompt_user_fuse_mode(callback, dir_path, img_name, fuse_status_signal):
    method_selection_interface = MethodDialog()
    method_selection_interface.decision_made.connect(lambda path: callback(path, dir_path, img_name, fuse_status_signal))
    method_selection_interface.exec()

