
import os
import cv2
from re import search
from PIL import Image
from numpy import average
from json import load, dump
from threading import Thread
from time import time, sleep
from dataclasses import dataclass
from logging.config import dictConfig
from logging import getLogger, DEBUG, INFO

from PyQt5.QtWidgets import (QApplication, QMainWindow, QLineEdit, QPushButton, QLabel, QStatusBar,
                            QDialog, QTreeView, QFileSystemModel, QFileDialog, QMessageBox, QTabWidget,
                            QSlider, QListWidgetItem, QListWidget, QSpinBox, QDoubleSpinBox, QMenu)
from PyQt5.QtGui import QIcon, QImage, QPixmap
from PyQt5.QtCore import Qt, QThread, pyqtSignal, QObject
from PyQt5 import uic

# disable all PyQt loggers
dictConfig({"version": 1, "disable_existing_loggers": True})

from Arducam.Arducam import ArducamCamera
from Arducam.ImageConvert import convert_image

import constants
import stacker
from MotorController.motorcontroller import Axis
from . import method_selection

logger = getLogger("UserInterface")
logger.setLevel(DEBUG)


def isfloat(string: str) -> bool:
    try:
        float(string)
        return True
    except ValueError:
        return False


@dataclass
class Lens:
    name: str
    lenght: int
    settings: list
    base_focus_height: float = None

    @property
    def label_str(self):
        return f"{self.name} - {self.lenght}mm"


class MessageBox():
    message_box: QMessageBox = None

    @staticmethod
    def information(text: str, abort=False, callback=None):
        MessageBox.message_box = QMessageBox()
        MessageBox.message_box.setWindowTitle("Information")
        MessageBox.message_box.setWindowIcon(QIcon(r"UserInterface\imgs\enimas_icon.png"))
        MessageBox.message_box.setIcon(QMessageBox.Icon.Information)
        MessageBox.message_box.setText(text)
        if abort:
            MessageBox.message_box.setStandardButtons(QMessageBox.StandardButton.Abort)
            MessageBox.message_box.buttonClicked.connect(callback)
        MessageBox.message_box.show()
        QApplication.processEvents()

    @staticmethod
    def hide_message():
        if MessageBox.message_box:
            MessageBox.message_box.done(1)


class MainWindow(QMainWindow):
    fuse_status = pyqtSignal(str)
    show_connection_error = pyqtSignal()
    
    def __init__(self, axis: Axis, camera: ArducamCamera):
        super().__init__()
        logger.debug("Starting interface")
        uic.loadUi("UserInterface/main.ui", self)

        self.axis: Axis = axis
        
        self._number_stacks = constants.DEFAULT_NUM_OF_STACKS
        self._stack_step_size = constants.DEFAULT_STACK_STEP_SIZE

        self.setWindowFlags(Qt.WindowType.Window)
        self.setWindowIcon(QIcon(r"UserInterface\imgs\enimas_icon.png"))
        self.setWindowTitle("ENIMAS - Entomoscope Imaging Software")
        self.statusBar: QStatusBar

        # Find Widgets from .ui file
        self.stack_speed_spinbox: QSpinBox = self.findChild(QSpinBox, "stack_speed_spinbox")
        self.number_stacks_spinbox: QSpinBox = self.findChild(QSpinBox, "number_stacks_spinbox")
        self.step_size_spinbox: QDoubleSpinBox = self.findChild(QDoubleSpinBox, "step_size_spinbox")
        
        self.take_single_picture_bttn: QPushButton = self.findChild(QPushButton, "take_single_picture_bttn")
        self.take_stack_picture_bttn: QPushButton = self.findChild(QPushButton, "take_stack_picture_bttn")

        self.motor_up_button = self.findChild(QPushButton, "motor_up_button")
        self.motor_down_button = self.findChild(QPushButton, "motor_down_button")
        self.autofocus_button = self.findChild(QPushButton, "autofocus_button")
        self.motor_stop_button = self.findChild(QPushButton, "motor_stop_button")
        self.motor_reference_button = self.findChild(QPushButton, "motor_reference_button")
        
        self.cam_setting_bttn = self.findChild(QPushButton, "cam_settings_bttn")
        
        # Directory
        dir_widgets = [
            self.findChild(QTreeView, "dir_view"),
            self.findChild(QPushButton, "browse_dir_bttn"),
            self.findChild(QPushButton, "add_specimen_bttn"),
            self.findChild(QLineEdit, "entry_1"),
            self.findChild(QLineEdit, "entry_2"),
            self.findChild(QSpinBox, "spinBox"),
            self.findChild(QTabWidget, "tabWidget"),
            self.findChild(QLineEdit, "single_specimen_entry"),
            self.findChild(QLineEdit, "single_folder_entry"),
            self.findChild(QPushButton, "browse_single_dir_bttn")
        ]
        self.directory_manager = DirectoryManager(dir_widgets, self.statusBar)

        # Video label
        self.video_label = self.findChild(QLabel, "video_label")
        self.show_connection_error.connect(self.camera_connection_error)
        self.arducam = Arducam(camera, self.video_label, self.show_connection_error)

        # Lens Manager
        self.change_lens_bttn = self.findChild(QPushButton, "change_lens_bttn")
        self.lens_info_label = self.findChild(QLabel, "lens_info_label")
    
        self.all_lenses = []
        self.lens_approved = False
        self.current_lens: Lens = None
        self.default_cam_values = self.arducam.get_reg_values()
        self.load_lenses_from_file()
        self.lens_info_label.setText(self.current_lens.label_str)
        
        # Set default stack values
        self.stack_speed_spinbox.setValue(constants.STACK_SPEED)
        self.number_stacks_spinbox.setValue(self._number_stacks)
        self.step_size_spinbox.setValue(self._stack_step_size)
        
        # Connect callback methods
        self.motor_up_button.clicked.connect(self.motor_up)
        self.motor_down_button.clicked.connect(self.motor_down)
        self.autofocus_button.clicked.connect(self.autofocus_clicked)
        self.motor_reference_button.clicked.connect(self.motor_ref)
        self.motor_stop_button.clicked.connect(self.motor_stop)
        self.take_single_picture_bttn.clicked.connect(self.take_single_picture)
        self.take_stack_picture_bttn.clicked.connect(self.stack_pictures_clicked)
        self.change_lens_bttn.clicked.connect(self.open_lens_dialog)
        self.cam_setting_bttn.clicked.connect(self.open_settings_dialog)
        
        self.number_stacks_spinbox.valueChanged.connect(self.number_stacks_changed)
        self.step_size_spinbox.valueChanged.connect(self.step_size_changed)
        
        # change status tips when values for folder names changed
        dir_widgets[0].clicked.connect(self.set_status_tip) # explorer
        dir_widgets[3].textChanged.connect(self.set_status_tip) # code prefix
        dir_widgets[4].textChanged.connect(self.set_status_tip) # plate id
        dir_widgets[5].valueChanged.connect(self.set_status_tip) # code number
        dir_widgets[6].currentChanged.connect(self.set_status_tip) # tab
        dir_widgets[7].textChanged.connect(self.set_status_tip) # single name
        
        # StatusBar tips
        self.set_status_tip()
        
        # Stack fuse status in statusbar
        self.fuse_status.connect(self.statusBar.showMessage)
        
        # little menu when right clicking stack img button
        self.take_stack_picture_bttn.setContextMenuPolicy(Qt.CustomContextMenu)
        self.take_stack_picture_bttn.customContextMenuRequested.connect(self.change_stack_mode_menu)
        
        # Disable motor movement until referenced
        self.set_motor_control_state(False)
        self.motor_reference_button.setEnabled(True)
    
    
    def motor_up(self, *, steps: float=None, wait=False):
        if steps is None:
            steps = self._stack_step_size
        logger.debug(f"moving up for {steps}") 
        self.axis.move_for(-steps, wait=wait)

    def motor_down(self, *, steps: float=None, wait=False):
        if steps is None:
            steps = self._stack_step_size
        logger.debug(f"moving down for {steps}") 
        self.axis.move_for(steps, wait=wait)
    
    def move_motor_to(self, pos: float, wait=False):
        logger.debug(f"moving to {pos}") 
        self.axis.set_speed(constants.DEFAULT_SPEED)
        self.axis.move_to(pos, wait=wait)
        self.axis.set_speed(constants.LOW_SPEED)

    def motor_ref(self):
        logger.debug("referencing")
        self.set_motor_control_state(False)
        MessageBox.information("Referencing Axis ...   ")
        self.axis.axis_reference()
        MessageBox.hide_message()
        self.set_motor_control_state(True)
    
    def motor_stop(self):
        logger.debug("stopping motor")
        self.axis.stop()
        self.stop = True # to stop the stack loop or autofocus loop if it's running

    def set_motor_control_state(self, state):
        self.motor_up_button.setEnabled(state)
        self.motor_down_button.setEnabled(state)
        self.autofocus_button.setEnabled(state)
        self.motor_reference_button.setEnabled(state)
        self.take_stack_picture_bttn.setEnabled(state)

    def approve_lens(self, lens: Lens, text) -> bool:
        # answer = QMessageBox.question(self, "Confirm the current Lens", f"Are you sure that the current lens is: {lens.name}, with a lenght of {lens.lenght}mm ? \n{text}")
        answer = QMessageBox.warning(self, "Confirm the current Lens", f"Are you sure that the current lens is: {lens.name}, with a lenght of {lens.lenght}mm ? \n{text}", buttons= QMessageBox.Yes | QMessageBox.No)
        return answer == QMessageBox.Yes
    
    
    def autofocus_clicked(self):
        if not self.lens_approved:
            if not self.approve_lens(self.current_lens, "The camera is going to move to the lowest point."):
                return 
            self.lens_approved = True
        
        MessageBox.information("Autofocusing ...", abort=True, callback=self.motor_stop)
        self.set_motor_control_state(False)
        QApplication.processEvents()
        Thread(target=self.do_autofocus).start()
        
    def do_autofocus(self):
        logger.debug("autofocusing...")

        self.stop = False
        in_focus_area = False
        sharpnesses = []
        step_size = 3

        self.move_motor_to(self.axis.lower_limit - 20, wait=True)
        
        while self.axis.z - step_size > 0 and not self.stop:
            self.motor_up(steps=step_size, wait=True)
            sharpness = self.arducam.calc_focus(self.arducam.get_image())

            logger.debug(f"{sharpness=}")
            sharpnesses.append(sharpness)

            if sharpness > 60 and not in_focus_area:
                logger.debug("enter focus zone")
                step_size = .5
                in_focus_area = True

            if in_focus_area and len(sharpnesses) > 3 and sharpnesses[-1] < sharpnesses[-2] < sharpnesses[-3]:
                # now on the focus point
                
                self.motor_down(steps=3*step_size)
                if self.current_lens:
                    # save the base focus height
                    self.current_lens.base_focus_height = 422 - (self.axis.z + 59 + self.current_lens.lenght)
                    self.dump_lenses_to_file()
                    
                break
            
        else:
            self.stop = False
        

        MessageBox.hide_message()
        self.set_motor_control_state(True)

    def number_stacks_changed(self):
        self._number_stacks = self.number_stacks_spinbox.value()
    
    def step_size_changed(self):
        self._stack_step_size = self.step_size_spinbox.value()

    def set_status_tip(self):
        # StatusTip for single picture button
        if path := self.directory_manager.add_image(create_dir=False):
            self.take_single_picture_bttn.setStatusTip(f"Save a single picture in {os.path.abspath(path)}")
        else:
            self.take_single_picture_bttn.setStatusTip("Save a single picture")
        
        # StatusTip for stack picture button
        if img_params := self.directory_manager.add_stack_image(5, create_dir=False):
            self.take_stack_picture_bttn.setStatusTip(f"Save a stack picture in {img_params[2]}")
        else:
            self.take_stack_picture_bttn.setStatusTip("Save a stack picture")

    def take_single_picture(self):
        img = self.arducam.get_image()
        img_file_path, img_dir = self.directory_manager.add_image(get_dir=True)
        
        if img_file_path is None:
            if self.directory_manager.current_dir_mode == 0:
                QMessageBox.critical(self, "Couldn't take a picture.", "No folder names are specified to save a picture. \nPlease enter a Specimen prefix, Plate ID and Specimen code number.")
            else:
                QMessageBox.critical(self, "Couldn't take a picture.", "No specimen name. \nPlease enter a folder path and a specimen name to save a picture.")
            return
            
        if not os.path.isdir(img_dir): os.makedirs(img_dir)
            
        response = cv2.imwrite(img_file_path, img)
        if not response:
            QMessageBox.critical(self, "Saving the picture failed", f"Something went wrong while saving the picture in:\n{img_file_path}")
        self.set_status_tip()
        
    def stack_pictures_clicked(self):
        img_params = self.directory_manager.add_stack_image(self._number_stacks)
        if img_params is None:
            if self.directory_manager.current_dir_mode == 0:
                QMessageBox.critical(self, "Couldn't take a stack picture.", "No folder names are specified to save a stack picture. \nPlease enter a Specimen prefix, Plate ID and Specimen code number.")
            else:
                QMessageBox.critical(self, "Couldn't take a stack picture.", "No specimen name. \nPlease enter a specimen name to save a stack picture.")
            return
            
        if not self.axis.check_limit(-1 * self._number_stacks * self._stack_step_size):
            self.statusBar.showMessage("Stacking not possible. The highest position is out of limit")
            return
            
        MessageBox.information("Taking stacking pictures. Please don't move.", abort=True, callback=self.motor_stop)
        self.set_motor_control_state(False)
        QApplication.processEvents()
        
        
        class Stack(QObject):
            stack_done = pyqtSignal()

            def __init__(self, main, paths):
                super().__init__()
                self.main: MainWindow = main
                self.paths = paths
            
            def run(self):
                """takes {self._numberStacks} pictures with a distance of {self._stackStepSize}"""
                logger.info("Stack thread running")
                self.main.stop = False

                imgs = []
                speed = self.main.stack_speed_spinbox.value() * constants.LOW_SPEED / 100
                self.main.axis.set_speed(speed)
                
                for step in range(self.main._number_stacks):
                    if self.main.stop: break
                    
                    imgs.append(self.main.arducam.take_image())
                    self.main.motor_up(wait=True)
                
                self.main.axis.set_speed(constants.LOW_SPEED)
                if not self.main.stop:
                    self.main.motor_down(steps=self.main._number_stacks*self.main._stack_step_size, wait=True)
                self.main.stop = False
                
                self.main.set_motor_control_state(True)
                MessageBox.hide_message()
                
                for img, img_name in zip(imgs, self.paths[0]):
                    cv2.imwrite(img_name, img)
                
                logger.info("Stack thread ending")
                self.stack_done.emit()
        
        
        stack = Stack(self, img_params)
        stack.stack_done.connect(lambda: stacker.fuse_stacks(img_params[1], img_params[2], self.fuse_status))
        
        stacking_thead = Thread(target=stack.run)
        stacking_thead.start()
    
    def change_stack_mode_menu(self, pos):
        menu = QMenu()
        
        change_option = menu.addAction("Change fuse method...")
        change_option.triggered.connect(self.change_fuse_method)
        
        menu.exec(self.take_stack_picture_bttn.mapToGlobal(pos))
        
    def change_fuse_method(self):
        selection_dialog = method_selection.MethodDialog()
        selection_dialog.decision_made.connect(lambda path: constants.__setattr__("HELICON_PATH", path))
        selection_dialog.exec()
    
    def load_lenses_from_file(self):
        """Loads the lens informations from the JSON file."""
        if not "lenses.json" in os.listdir():
            # create a default lens
            self.all_lenses.append(Lens("Default lens", 150, self.default_cam_values))
            self.current_lens = self.all_lenses[0]
        else:
            with open("lenses.json", "r") as file:
                json_file = load(file)
                
                if not "lenses" in json_file:
                    self.all_lenses.append(Lens("Default lens", 150, self.default_cam_values))
                else:
                    for lens in json_file["lenses"]:
                        self.all_lenses.append(Lens(**lens))
                
                if not "current" in json_file:
                    self.current_lens = self.all_lenses[0]
                else:
                    self.current_lens = [lens for lens in self.all_lenses if lens.name == json_file["current"]][0]
        
        self.set_settings(self.current_lens.settings)
        self.set_new_limit()

    def dump_lenses_to_file(self):
        """Writes the list of Lenses to a JSON file with their camera settings"""
        with open("lenses.json", "w") as file:
            dump({"lenses": [lens.__dict__ for lens in self.all_lenses], "current": self.current_lens.name}, file, indent=4)
   
    def open_lens_dialog(self):
        """Opens a QDialog to change or add new lenses"""

        if "lens_dialog" in self.__dict__: 
            # close the other window if there is any
            self.lens_dialog.close()

        self.lens_dialog = LensDialog(self)
        self.lens_dialog.show()

    def update_lens_list(self, new_list):
        """Called from the QDialog with a new list of lenses"""
        self.all_lenses = new_list
    
    def set_new_limit(self):
        """Updates the soft limit with the current lens lenght"""
        new_limit = 422 - (self.current_lens.lenght + 59) - 20 # see constants.py
        self.axis.set_current_limit(new_limit)

    def set_current_lens(self, lens: Lens):
        """called after the lens changed or after a new one has been added"""
        is_new_lens = (self.current_lens != lens)
        
        self.current_lens = lens
        self.lens_info_label.setText(lens.label_str)
        
        # load the settings of the just selected lens
        self.set_settings(self.current_lens.settings)
        
        self.set_new_limit()
        self.dump_lenses_to_file()
        
        if is_new_lens and self.current_lens.base_focus_height is not None:
            self.lens_approved = self.approve_lens(lens, "The camera is going to move to the last saved focus point.")
            if not self.lens_approved:
                return
            
            # a base focus height has been saved for this lens
            # the motor will be moved to this focus height
            # focus_height = 422 - (59 + self.current_lens.lenght + self.current_lens.base_focus_height)
            
            MessageBox.information(f"Moving camera to the focus point of lens: {self.current_lens.name}", abort=True, callback=self.motor_stop)
            QApplication.processEvents()
            self.set_motor_control_state(False)
            Thread(target = self.move_to_focus_hight).start()
    
    def move_to_focus_hight(self):
        focus_height = 422 - (59 + self.current_lens.lenght + self.current_lens.base_focus_height)
        self.move_motor_to(focus_height, wait=True)
        MessageBox.hide_message()
        self.set_motor_control_state(True)

    def set_settings(self, settings):
        """Sets the given camera settings in the ArduCam"""
        self.arducam.camera.set_exposure_time(settings[0])
        self.arducam.camera.set_analog_gain(settings[1])
        self.arducam.camera.set_wb_gains_factor(settings[2], settings[3], settings[4])

    def open_settings_dialog(self):
        """Opens the camera settings dialog with default or current settings"""
        change_methods = {"gain": self.arducam.camera.set_analog_gain, 
                         "exp": self.arducam.camera.set_exposure_time,
                         "wb": self.arducam.camera.set_wb_gains_factor,
                         "auto_wb": self.arducam.white_balance,
                         "auto_exp": self.arducam.exposure_time}
        
        if "settings_dialog" in self.__dict__: 
            # close the other window if there is any
            self.settings_dialog.close()
        
        self.settings_dialog =   SettingsDialog(self, self.current_lens.settings, change_methods)
        self.settings_dialog.show() 

    def save_cam_values(self, values):
        """Called after the settings have been changed"""
        if self.current_lens is not None:
            # change the settings of the current lens
            for lens in self.all_lenses:
                if lens.name == self.current_lens.name:
                    lens.settings = values
                    self.current_lens.settings = values
        # save the new settings in the json file
        self.dump_lenses_to_file()

    def camera_connection_error(self):
        QMessageBox.critical(self, "ENIMAS: Camera error", "The connection to the Entomoscope camera failed.\nPlease restart the program.")
        self.close()

    def closeEvent(self, event):
        logger.info("Window is closing.")
        
        try:
            self.motor_stop()
            self.axis.__del__()
            self.arducam.stop()
        except Exception as exception:
            logger.error(exception)
        
        # save the user inputs to constants.py file, then it will be saved in config.txt
        constants.STACK_SPEED = self.stack_speed_spinbox.value()
        constants.DEFAULT_NUM_OF_STACKS = self.number_stacks_spinbox.value()
        constants.DEFAULT_STACK_STEP_SIZE = self.step_size_spinbox.value()
        constants.DEFAULT_BASE_DIR = self.directory_manager.base_dir
        
        event.accept()


class Arducam(QThread):
    def __init__(self, camera: ArducamCamera, video_label, show_error):
        super().__init__()
        """ Thread that reads the camera input everytime possible
        https://github.com/ArduCAM/ArduCAM_USB_Camera_Shield_Python_Demo """
        
        self.show_error = show_error
        self.video_label = video_label
        self.current_img = None
        self._run_flag = True
        self.camera = camera
        
        self.color_mode = self.camera.color_mode

        self.start()
    
    def get_reg_values(self):
        return self.camera.read_reg_values()

    def read_image(self):
        try:
            return self.camera.read()
        except RuntimeError:
            self.show_error.emit()
            sleep(5)
            raise
        

    def take_image(self):
        t0 = time()
        ret, data, cfg = self.read_image()
        
        if data is None:
            return None
            
        while not ret:
            ret, data, cfg = self.read_image()
            
            if time() - t0 > 5.0:
                # timeout
                logger.error("Connection to camera failed")
                self.show_error.emit()
                return None
                # raise ConnectionError("Connection to camera failed")
                    
        return cv2.flip(convert_image(data, cfg, self.color_mode), 1)

    def run(self):
        while self._run_flag:
            image = self.take_image()
            if image is not None:
                self.update_image(image)

    def update_image(self, cv_img):
        """Updates the image_label with a new opencv image"""
        qt_img = self.convert_cv_qt(cv_img)
        self.video_label.setPixmap(qt_img)
        self.current_img = cv_img
    
    def convert_cv_qt(self, cv_img):
        """ Converts an opencv image to QPixmap
        https://github.com/docPhil99/opencvQtdemo/blob/master/liveLabel3.py """

        rgb_image = cv2.cvtColor(cv_img, cv2.COLOR_BGR2RGB)
        h, w, ch = rgb_image.shape
        bytes_per_line = ch * w
        convert_to_Qt_format = QImage(rgb_image.data, w, h, bytes_per_line, QImage.Format.Format_RGB888)
        width, height = self.video_label.width(), self.video_label.height()
        p = convert_to_Qt_format.scaled(width, height, Qt.AspectRatioMode.KeepAspectRatio)
        return QPixmap.fromImage(p)

    def get_image(self):
        if self.current_img is None:
            raise Exception("Connection to camera failed.")
        return self.current_img
     
    @staticmethod
    def calc_focus(image):
        # compute the Laplacian of the image and then return the focus
        # measure, which is simply the variance of the Laplacian
        factor = 10
        resized_img = cv2.resize(image, (int(image.shape[1]/factor), int(image.shape[0]/factor)))
        
        gray = cv2.cvtColor(resized_img, cv2.COLOR_BGR2GRAY)
        return cv2.Laplacian(gray, cv2.CV_64F).var()

    def white_balance(self):
        """White balance method:
        compute the individual analog gain factor for each channel 
        return: register values
        """
        # write 100 to all 3 channel gain register to norm it
        rr, rg, rb = 100, 100, 100
        self.camera.set_wb_gains_factor(rr, rg, rb)
        
        # compute the factors for each channel to get a gray image
        # repeat until values don't change anymore
        logger.debug(f"{rr}, {rg}, {rb}")
        sleep(.2)
        img = self.take_image()
        width, height = img.shape[1], img.shape[0]
        img = img[int(height/2-20):int(height/2+20), int(width/2-20):int(width/2+20)]
        
        for i in range(3):
            b, g, r = average(average(img, axis=0), axis=0)
            max_ = max(r, g, b)
            rf, gf, bf = max_/r, max_/g, max_/b
            rr, rg, rb = int(rr*rf), int(rg*gf), int(rb*bf)
            self.camera.set_wb_gains_factor(rr, rg, rb)
            logger.debug(f"White Balance: {i}:  {int(r)}, {int(g)}, {int(b)};    {rr}, {rg}, {rb}")
            sleep(.2)
            img = self.take_image()
            img = img[int(height/2-25):int(height/2+25), int(width/2-20):int(width/2+20)]
            
        b, g, r = average(average(img, axis=0), axis=0)
        logger.debug(f"White Balance: end: {int(r)}, {int(g)}, {int(b)}")
        
        if max(r, g, b) > 230:
            # The image is too bright
            logger.debug("white balance: retry with new exposure time")
            self.exposure_time()
            return self.white_balance()
            
        if min(r, g, b) < 50 or max(r,g,b)/min(r,g,b) >= 1.05:
            # The image is too dark or the image is not white (gray)
            answer = QMessageBox.information(self.video_label, 
                                             "White balance failed", 
                                             "Auto white-balancing failed. \nPlease make sure that the center of the image is empty and the light is on.", 
                                             QMessageBox.StandardButton.Retry | QMessageBox.StandardButton.Cancel, 
                                             QMessageBox.StandardButton.Retry)
            if answer == QMessageBox.StandardButton.Retry:
                return self.white_balance()
            
        return rr, rg, rb
    
    def exposure_time(self):
        val_beginning = 4*1e4
        self.camera.set_analog_gain(1)
        self.camera.set_exposure_time(val_beginning)
        sleep(.2)
        img = self.take_image()
        
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        average_ = average(average(img, axis=0), axis=0)
        new_val = int(val_beginning * 150 / average_)
        logger.debug(f"Exposure Time: {average_=}, {new_val=}")
        self.camera.set_exposure_time(new_val)
        return new_val
    
    def stop(self):
        self._run_flag = False
        self.wait()
        try:
            self.camera.stop()
            self.camera.closeCamera()
        except RuntimeError:
            pass


class DirectoryManager:
    def __init__(self, dir_widgets, statusBar):
        self.explorer, self.browse_dir_bttn, self.add_specimen_bttn, self.prefix_entry, self.plate_id_entry, self.code_number_entry, self.tab_widget, self.single_specimen_entry, self.single_folder_entry, self.browse_single_dir_bttn = dir_widgets
        self.statusBar: QStatusBar = statusBar
        self.tab_widget: QTabWidget
        
        self.explorer.clicked.connect(self.explorer_clicked)
        self.browse_dir_bttn.clicked.connect(self.browse_dir_clicked)
        self.browse_single_dir_bttn.clicked.connect(self.browse_single_dir_clicked)
        self.add_specimen_bttn.clicked.connect(self.add_specimen)
        self.tab_widget.currentChanged.connect(self.new_tab_selected)
        
        self.current_dir_mode: int = self.tab_widget.currentIndex()
        self.base_dir = constants.DEFAULT_BASE_DIR
        self.init_folder_names()

        self.model = QFileSystemModel()
        self.model.setRootPath(self.base_dir)
        self.explorer.setModel(self.model)
        self.explorer.setRootIndex(self.model.index(self.base_dir))
        self.explorer.setColumnWidth(0, 920)

    def init_folder_names(self) -> None:
        """Inserts latest folder names or creates new ones"""
        
        if not os.path.isdir(self.base_dir):
            os.makedirs(self.base_dir)
        
        # Structure Tab
        prefix_init, plate_init, code_init = "", "", 0

        if prefixes := [dir for basename in os.listdir(self.base_dir) if os.path.isdir(dir := os.path.join(self.base_dir, basename)) and not "_RAW_Data_" in basename]:
            latest = max(prefixes, key=os.path.getctime)
            prefix_init = os.path.basename(latest)

            if plates := [dir for basename in os.listdir(latest) if os.path.isdir(dir := os.path.join(latest, basename)) and not "_RAW_Data_" in basename]:
                latest = max(plates, key=os.path.getctime)
                plate_init = os.path.basename(latest)

                if codes := [dir for basename in os.listdir(latest) if basename.isdigit() and os.path.isdir(dir := os.path.join(latest, basename)) and not "_RAW_Data_" in basename]:
                    latest = max(codes, key=os.path.getctime)
                    code_init = os.path.basename(latest)

        self.prefix_entry.setText(prefix_init)
        self.plate_id_entry.setText(plate_init)
        try:
            self.code_number_entry.setValue(int(code_init))
        except ValueError:
            logger.error("Code number was not an int while inserting for init.")
        
        
        # Single Tab
        self.single_folder_entry.setText(self.base_dir)
        

    def new_tab_selected(self):
        """Slot called after Tab changed"""
        self.current_dir_mode = self.tab_widget.currentIndex()
        
        if self.current_dir_mode:
            if os.path.isdir(self.single_folder_entry.text()):
                self.explorer.setRootIndex(self.model.index(self.single_folder_entry.text()))
        else:
            self.explorer.setRootIndex(self.model.index(self.base_dir))

    def browse_dir_clicked(self):
        """Open File dialog to select a new base directory"""
        dialog = QFileDialog()
        selected_dir = dialog.getExistingDirectory(self.explorer, "Select a base directory to save the folder structure in.", self.base_dir)
        self.base_dir = selected_dir
        self.explorer.setRootIndex(self.model.index(self.base_dir))

    def browse_single_dir_clicked(self):
        """Open File dialog to select a new single folder for the single image file"""
        dialog = QFileDialog()
        selected_dir = dialog.getExistingDirectory(self.explorer, "Select a folder to save individual images.", self.base_dir)
        self.single_folder_entry.setText(selected_dir)
        self.explorer.setRootIndex(self.model.index(selected_dir))

    def explorer_clicked(self, clicked_folder):
        """Shows the folder name clicked in the explorer to save next imgs in or opens the clicked image in the standart image viewing app"""
        path = self.model.filePath(clicked_folder)
        self.statusBar.clearMessage()
        
        if os.path.isfile(path) and path.endswith(constants.IMG_EXTENSION):
            # Image clicked, open it 
            Image.open(path).show()
        
        if not os.path.isdir(path):
            self.statusBar.showMessage("Clicked file is neither a directory nor an image")
            return
        
        if self.current_dir_mode == 1:
            # Nothing happend when in single image saving mode
            return
        
        new_prefix, new_plate, new_code = None, None, None

        level = len(path.split("/")) - len(os.path.abspath(self.base_dir).split("\\"))

        if level == 1:
            # a prefix folder was clicked
            new_prefix = os.path.basename(path)
            if plates := [plate_dir for plate_basename in os.listdir(path) if os.path.isdir(plate_dir := os.path.join(path, plate_basename))]:
                path = max(plates, key=os.path.getctime)
                level = 2
        
        if level == 2:
            # a plate_id folder was clicked
            new_prefix = os.path.basename(os.path.dirname(path))
            new_plate = os.path.basename(path)
            if codes := [code_dir for code_basename in os.listdir(path) if code_basename.isdigit() and os.path.isdir(code_dir := os.path.join(path, code_basename))]:
                path = max(codes, key=os.path.getctime)
                level = 3

        if level == 3:
            # a code_number folder was clicked
            new_prefix = os.path.basename(os.path.dirname(os.path.dirname(path)))
            new_plate = os.path.basename(os.path.dirname(path))
            if not (new_code := os.path.basename(path)).isdigit(): 
                new_code = 0
        
        if level > 3:
            self.statusBar.showMessage("Clicked folder is not a Prefix folder, Plate ID folder or a Code number folder.")

        if new_code: self.code_number_entry.setValue(int(new_code))
        if new_plate: self.plate_id_entry.setText(new_plate)
        if new_prefix: self.prefix_entry.setText(new_prefix)

    def add_specimen(self):
        """Adds 1 to the specimen code number (should be an integer)"""
        code_number = self.code_number_entry.value()
        self.code_number_entry.setValue(code_number + 1)
    
    def get_current_folder_name(self, create_dir=True) -> str:
        """Checks the user input and created these files if they dont exist. Returns an existing folder and the name to save an image in"""
        
        if self.current_dir_mode == 1:
            # single file mode
            folder_name = self.single_folder_entry.text()
            if not os.path.isabs(folder_name): 
                folder_name = os.path.join(self.base_dir, folder_name)
            
            specimen_name = self.single_specimen_entry.text()
            if not (specimen_name and folder_name):
                return None, None
            return folder_name, specimen_name
        
        prefix = self.prefix_entry.text()
        plate_id = self.plate_id_entry.text()
        code_number = self.code_number_entry.value()
        if not all([prefix, plate_id, code_number]):
            return None, None

        code_number = f"{code_number:0>7}"
        specimen_name = f"{plate_id}_{prefix}{code_number}"
        path_to_specimen_folder = os.path.join(self.base_dir, prefix, plate_id, f"{prefix}{code_number}")
        
        if not os.path.isdir(path_to_specimen_folder) and create_dir:
            try:
                os.makedirs(path_to_specimen_folder)
            except Exception as exception:
                QMessageBox.critical(self, "Saving images failed", str(exception) + "\nSaving images in desired folder failed. Images will be saved in the default Images folder")
                self.base_dir = constants.DEFAULT_BASE_DIR
                self.explorer.setRootIndex(self.model.index(self.base_dir))
                return self.get_current_folder_name()

        return path_to_specimen_folder, specimen_name
    
    def add_image(self, create_dir=True, get_dir=False) -> str:
        """Returns a path to a new image file in the given specimen directory"""
        img_path, specimen_name = self.get_current_folder_name(create_dir)
        if img_path is None:
            return None if not get_dir else (None, None)

        # image files
        files = [file for file in os.listdir(img_path) if os.path.isfile(os.path.join(img_path, file)) and file.startswith(specimen_name)] if os.path.isdir(img_path) else []
        if files:
            # get the latest img file and create a new one 
            all_numbers = [int(match.group(1)) for name in files if (match := search(f"^{specimen_name}_(\d+).{constants.IMG_EXTENSION}$", name))]
            highest = max(all_numbers) if all_numbers else 0
            new_img_name = f"{specimen_name}_{highest+1:0>3}.{constants.IMG_EXTENSION}"
        else:
            # it's the first img file in this directory
            new_img_name = f"{specimen_name}_001.{constants.IMG_EXTENSION}" if self.current_dir_mode == 0 else f"{specimen_name}.{constants.IMG_EXTENSION}"
            
        if get_dir:
            return os.path.join(img_path, new_img_name), img_path 
        else:
            return os.path.join(img_path, new_img_name)

    def add_stack_image(self, number_of_stacks, create_dir=True) -> list:
        """Returns a list of image paths to save stack images in"""
        img_path, specimen_name = self.get_current_folder_name(create_dir)
        if img_path is None:
            return None
        
        # get all the stack folders
        dirs = [int(match.group(1)) for dir in os.listdir(img_path) if os.path.isdir(os.path.join(img_path, dir)) and (match := search(f"^{specimen_name}_RAW_Data_(\d+)$", dir))] if os.path.isdir(img_path) else []
        if dirs:
            highest = max(dirs)
            new_dir = f"{specimen_name}_RAW_Data_{highest+1:0>2}"
        else:
            highest = 0
            new_dir = f"{specimen_name}_RAW_Data_01"

        new_dir_path = os.path.join(img_path, new_dir)
        if create_dir: 
            os.makedirs(new_dir_path)
        
        img_names = [os.path.join(new_dir_path, f"{specimen_name}_RAW_{highest+1:0>2}_{i+1:0>2}.{constants.IMG_EXTENSION}") for i in range(number_of_stacks)]
        stacked_name = os.path.join(img_path, f"{specimen_name}_stacked_{highest+1:0>2}.{constants.IMG_EXTENSION}")
        return [img_names, new_dir_path, stacked_name]


class LensDialog(QDialog):
    def __init__(self, main: MainWindow):
        super().__init__(main)

        uic.loadUi("UserInterface/dialog.ui", self)
        self.setWindowIcon(QIcon(r"UserInterface\imgs\enimas_icon.png"))
        
        self.main_window = main
        self.all_lenses = main.all_lenses

        self.lens_list_widget: QListWidget = self.findChild(QListWidget, "lens_list")
        self.move_up_bttn = self.findChild(QPushButton, "move_up_bttn")
        
        self.name_edit = self.findChild(QLineEdit, "name_edit")
        self.lenght_edit = self.findChild(QLineEdit, "lenght_edit")
        self.distance_edit = self.findChild(QLineEdit, "dist_edit")
        self.new_bttn = self.findChild(QPushButton, "new_bttn")
        
        self.delete_bttn = self.findChild(QPushButton, "delete_bttn")
        self.status_bar: QStatusBar = self.findChild(QStatusBar, "status_bar")
        self.ok_bttn = self.findChild(QPushButton, "ok_bttn")
        self.cancel_bttn = self.findChild(QPushButton, "cancel_bttn")

        self.delete_bttn.setEnabled(False)

        self.move_up_bttn.clicked.connect(self.move_up_clicked)
        self.new_bttn.clicked.connect(self.new_clicked)
        self.ok_bttn.clicked.connect(self.ok_clicked)
        self.cancel_bttn.clicked.connect(self.close)
        self.lens_list_widget.itemClicked.connect(self.item_clicked)
        self.delete_bttn.clicked.connect(self.delete_item)

        for lens in self.all_lenses:
            # insert all the saved lenses into the QListWidget
            item = QListWidgetItem(lens.label_str)
            self.lens_list_widget.addItem(item)
            if lens == self.main_window.current_lens:
                self.lens_list_widget.setCurrentItem(item)
    
    def move_up_clicked(self):
        self.main_window.move_motor_to(10)
    
    def item_clicked(self, item):
        self.delete_bttn.setEnabled(True)
    
    def delete_item(self):
        lens_to_delete = self.lens_list_widget.currentItem()

        for lens in self.all_lenses:
            if lens.label_str == lens_to_delete.text():
                logger.info(f"delete item {lens.label_str}")
                self.all_lenses.remove(lens)

        row = self.lens_list_widget.row(lens_to_delete)
        self.lens_list_widget.takeItem(row)
        del lens_to_delete

        if not self.all_lenses:
            # all lenses habe been delete. Creating a default one
            default_lens = Lens("Default Lens", 150, self.main_window.default_cam_values)
            self.all_lenses.append(default_lens)

            item = QListWidgetItem(default_lens.label_str)
            self.lens_list_widget.addItem(item)
            self.lens_list_widget.setCurrentItem(item)

    def new_clicked(self):
        """A new Lens will be added"""
        self.status_bar.clearMessage()
        name = self.name_edit.text()
        lenght: str = self.lenght_edit.text()
        distance: str = self.distance_edit.text()
        
        if not (name and lenght):
            self.status_bar.showMessage("Please enter a name and a lenght for the new lens.")
            return
        if lenght.endswith("mm"):
            lenght = lenght[:-2]
        if not lenght.isdigit() or not (0 < int(lenght) <= 360):
            self.status_bar.showMessage(f"Invalid lenght for new lens: '{lenght}'")
            return
        if distance and (not distance.isdigit() or not (0 < int(distance) <= 360)):
            self.status_bar.showMessage(f"Invalid object distance for new lens: '{distance}'")
            return
        
        if name in [lens.name for lens in self.all_lenses]:
            self.status_bar.showMessage(f"Lens named {name} already exists")
            return

        # get the current cam setting from the previous lens (or the default ones)
        current_setting = self.main_window.default_cam_values
        new_lens = Lens(name, int(lenght), current_setting) if not distance else Lens(name, int(lenght), current_setting, float(distance))
        self.all_lenses.append(new_lens)

        item = QListWidgetItem(new_lens.label_str)
        self.lens_list_widget.addItem(item)
        self.lens_list_widget.setCurrentItem(item)

    def ok_clicked(self):
        """The selected lens will be the current lens."""
        selected_lens = self.lens_list_widget.currentItem()
        if not selected_lens:
            self.status_bar.showMessage("No lens selected.")
            return

        selected_lens = [lens for lens in self.all_lenses if lens.label_str == selected_lens.text()][0]
        self.close()
        self.main_window.update_lens_list(self.all_lenses)
        self.main_window.set_current_lens(selected_lens)


class SettingsDialog(QDialog):
    def __init__(self, main: MainWindow, value_list, change_methods):
        super().__init__(main)
        uic.loadUi("UserInterface/camera_settings.ui", self)
        self.setWindowIcon(QIcon(r"UserInterface\imgs\enimas_icon.png"))

        self.main = main
        self.values_beginning = value_list
        self.change_methods = change_methods

        self.sliders = [self.findChild(QSlider, "horizontalSlider"),
                        self.findChild(QSlider, "horizontalSlider_2"),
                        self.findChild(QSlider, "horizontalSlider_3"),
                        self.findChild(QSlider, "horizontalSlider_4"),
                        self.findChild(QSlider, "horizontalSlider_5")]

        self.entries = [self.findChild(QSpinBox, "spinBox"),
                        self.findChild(QSpinBox, "spinBox_2"),
                        self.findChild(QSpinBox, "spinBox_3"),
                        self.findChild(QSpinBox, "spinBox_4"),
                        self.findChild(QSpinBox, "spinBox_5")]

        self.ok_bttn = self.findChild(QPushButton, "ok_bttn")
        self.cancel_bttn = self.findChild(QPushButton, "cancel_bttn")
        self.auto_wb_bttn = self.findChild(QPushButton, "auto_wb_bttn")
        self.auto_exp_bttn = self.findChild(QPushButton, "auto_exp_bttn")
        
        self.ok_bttn.clicked.connect(self.save_values)
        self.cancel_bttn.clicked.connect(self.close)
        self.auto_exp_bttn.clicked.connect(self.auto_exp)
        self.auto_wb_bttn.clicked.connect(self.auto_wb)
        
        self.exp_time_setting = Setting(self.sliders[0], self.entries[0], constants.EXP_TIME_MIN, constants.EXP_TIME_MAX, self.change_methods["exp"], self.values_beginning[0])
        self.gain_setting = Setting(self.sliders[1], self.entries[1], constants.GAIN_MIN, constants.GAIN_MAX, self.change_methods["gain"], self.values_beginning[1])
        self.wb_R_setting = Setting(self.sliders[2], self.entries[2], constants.WB_MIN, constants.WB_MAX, self.change_wb, self.values_beginning[2])
        self.wb_G_setting = Setting(self.sliders[3], self.entries[3], constants.WB_MIN, constants.WB_MAX, self.change_wb, self.values_beginning[3])
        self.wb_B_setting = Setting(self.sliders[4], self.entries[4], constants.WB_MIN, constants.WB_MAX, self.change_wb, self.values_beginning[4])

    def save_values(self):
        values = [self.exp_time_setting.value, self.gain_setting.value, self.wb_R_setting.value, self.wb_G_setting.value, self.wb_B_setting.value]
        self.main.save_cam_values(values)
        self.close()
    
    def change_wb(self, _):
        values = self.wb_R_setting.value, self.wb_G_setting.value, self.wb_B_setting.value
        self.change_methods["wb"](*values)

    def auto_wb(self):
        rf, gf, bf = self.change_methods["auto_wb"]()
        
        self.wb_R_setting.set_value(rf)
        self.wb_G_setting.set_value(gf)
        self.wb_B_setting.set_value(bf)

    def auto_exp(self):
        exp = self.change_methods["auto_exp"]()
        
        self.gain_setting.set_value(1)
        self.exp_time_setting.set_value(exp)


class Setting:
    def __init__(self, slider, spinbox, min_val, max_val, callback, begin_value):
        self.slider: QSlider = slider
        self.spinbox: QSpinBox = spinbox
        self.change_method = callback

        self.value = begin_value
        self.spinbox.setRange(min_val, max_val)
        self.slider.setValue(self.value)
        self.spinbox.setValue(self.value)
        self.slider.valueChanged.connect(self.slider_value_changed)
        self.spinbox.valueChanged.connect(self.spinbox_value_changed)
    
    def slider_value_changed(self):
        self.value = self.slider.value()
        
        if not self.value == self.spinbox.value():
            # update the spinbox if it isn't the same value
            self.spinbox.setValue(self.value)
        
        self.change_method(self.value)
    
    def spinbox_value_changed(self):
        value = self.spinbox.value()
        self.slider.setValue(value)
    
    def set_value(self, value: int):
        self.slider.setValue(value)
    
    def get_value(self):
        return self.value
